﻿Public Class UTILMDTECH
    'IDOCNR
    'ZPNAME
    'VOLTLEVELDP
    'SERIALNO
    'OBIS
    'TYPEREFSN
    'CNTRNAME
    'DECPLCPRECNT
    'DECPLCSUFCNT
    'CONVERTER
    'CONVFACT
    'COMM
    'COMMTYPE
    'CTRL
    'CTRLTYPE
    'SMGATE
    'GATEADMIN
    'CTRLBOX
    'GASQUAL
    'PRESSURELEVEL
    'READMETHODM
    'METERTYPE
    'METERSIZEGAS
    'RATECNT
    'DIRECTION
    'MOUNTTYPE
    'ACQMEASVAL
    'TRANSREASON
    'RESPONSESTATUS

    Public Property IDOCNR As String
    Public Property ZPNAME As String
    Public Property VOLTLEVELDP As String
    Public Property SERIALNO As String
    Public Property OBIS As String
    Public Property TYPEREFSN As String
    Public Property CNTRNAME As String
    Public Property DECPLCPRECNT As String
    Public Property DECPLCSUFCNT As String
    Public Property CONVERTER As String
    Public Property CONVFACT As String
    Public Property COMM As String
    Public Property COMMTYPE As String
    Public Property CTRL As String
    Public Property CTRLTYPE As String
    Public Property SMGATE As String
    Public Property GATEADMIN As String
    Public Property CTRLBOX As String
    Public Property GASQUAL As String
    Public Property PRESSURELEVEL As String
    Public Property READMETHODM As String
    Public Property METERTYPE As String
    Public Property METERSIZEGAS As String
    Public Property RATECNT As String
    Public Property DIRECTION As String
    Public Property MOUNTTYPE As String
    Public Property ACQMEASVAL As String
    Public Property TRANSREASON As String
    Public Property RESPONSESTATUS As String
    Public Sub New()
        Me.IDOCNR = ""
        Me.ZPNAME = ""
        Me.VOLTLEVELDP = ""
        Me.SERIALNO = ""
        Me.OBIS = ""
        Me.TYPEREFSN = ""
        Me.CNTRNAME = ""
        Me.DECPLCPRECNT = ""
        Me.DECPLCSUFCNT = ""
        Me.CONVERTER = ""
        Me.CONVFACT = ""
        Me.COMM = ""
        Me.COMMTYPE = ""
        Me.CTRL = ""
        Me.CTRLTYPE = ""
        Me.SMGATE = ""
        Me.GATEADMIN = ""
        Me.CTRLBOX = ""
        Me.GASQUAL = ""
        Me.PRESSURELEVEL = ""
        Me.READMETHODM = ""
        Me.METERTYPE = ""
        Me.METERSIZEGAS = ""
        Me.RATECNT = ""
        Me.DIRECTION = ""
        Me.MOUNTTYPE = ""
        Me.ACQMEASVAL = ""
        Me.TRANSREASON = ""
        Me.RESPONSESTATUS = ""
    End Sub
    Public Sub New(ByVal IDOCNR As String)
        Me.New
        Me.IDOCNR = IDOCNR
    End Sub

    Public Function CreateParameters() As SqlClient.SqlParameter()
        Dim oParams As New List(Of SqlClient.SqlParameter)
        oParams.Add(New SqlClient.SqlParameter("@IDOCNR", Me.IDOCNR))
        oParams.Add(New SqlClient.SqlParameter("@ZPNAME", Me.ZPNAME))
        oParams.Add(New SqlClient.SqlParameter("@VOLTLEVELDP", Me.VOLTLEVELDP))
        oParams.Add(New SqlClient.SqlParameter("@SERIALNO", Me.SERIALNO))
        oParams.Add(New SqlClient.SqlParameter("@OBIS", Me.OBIS))
        oParams.Add(New SqlClient.SqlParameter("@TYPEREFSN", Me.TYPEREFSN))
        oParams.Add(New SqlClient.SqlParameter("@CNTRNAME", Me.CNTRNAME))
        oParams.Add(New SqlClient.SqlParameter("@DECPLCPRECNT", Me.DECPLCPRECNT))
        oParams.Add(New SqlClient.SqlParameter("@DECPLCSUFCNT", Me.DECPLCSUFCNT))
        oParams.Add(New SqlClient.SqlParameter("@CONVERTER", Me.CONVERTER))
        oParams.Add(New SqlClient.SqlParameter("@CONVFACT", Me.CONVFACT))
        oParams.Add(New SqlClient.SqlParameter("@COMM", Me.COMM))
        oParams.Add(New SqlClient.SqlParameter("@COMMTYPE", Me.COMMTYPE))
        oParams.Add(New SqlClient.SqlParameter("@CTRL", Me.CTRL))
        oParams.Add(New SqlClient.SqlParameter("@CTRLTYPE", Me.CTRLTYPE))
        oParams.Add(New SqlClient.SqlParameter("@SMGATE", Me.SMGATE))
        oParams.Add(New SqlClient.SqlParameter("@GATEADMIN", Me.GATEADMIN))
        oParams.Add(New SqlClient.SqlParameter("@CTRLBOX", Me.CTRLBOX))
        oParams.Add(New SqlClient.SqlParameter("@GASQUAL", Me.GASQUAL))
        oParams.Add(New SqlClient.SqlParameter("@PRESSURELEVEL", Me.PRESSURELEVEL))
        oParams.Add(New SqlClient.SqlParameter("@READMETHODM", Me.READMETHODM))
        oParams.Add(New SqlClient.SqlParameter("@METERTYPE", Me.METERTYPE))
        oParams.Add(New SqlClient.SqlParameter("@METERSIZEGAS", Me.METERSIZEGAS))
        oParams.Add(New SqlClient.SqlParameter("@RATECNT", Me.RATECNT))
        oParams.Add(New SqlClient.SqlParameter("@DIRECTION", Me.DIRECTION))
        oParams.Add(New SqlClient.SqlParameter("@MOUNTTYPE", Me.MOUNTTYPE))
        oParams.Add(New SqlClient.SqlParameter("@ACQMEASVAL", Me.ACQMEASVAL))
        oParams.Add(New SqlClient.SqlParameter("@TRANSREASON", Me.TRANSREASON))
        oParams.Add(New SqlClient.SqlParameter("@RESPONSESTATUS", Me.RESPONSESTATUS))
        Return oParams.ToArray
    End Function
End Class
