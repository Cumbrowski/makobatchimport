﻿Public Class MSCONS
    'IDOCNR
    'IDOCCONT
    'EDITASK
    'VERSION
    'EDIREFUNB
    'MSGREFUNH
    'KAT
    'CHKIDENT
    'MSGDATE
    'USEREFUNB
    'MSGFUNC
    'MPIDMR
    'MPIDMS
    'ORDERSREF
    'CREATEDDTE
    'CREATEDBY
    'CHANGEDDTE
    'CHANGEDBY
    'ZPNAME
    'READDTE
    'OBIS
    'OBISTYPE
    'AG
    'AA
    'READHW
    'CCIACH
    'CCI16
    'QTY
    'QTYQUAL
    'STATEADDINFO

    Public Property IDOCNR As String
    Public Property IDOCCONT As String
    Public Property EDITASK As String
    Public Property VERSION As String
    Public Property EDIREFUNB As String
    Public Property MSGREFUNH As String
    Public Property KAT As String
    Public Property CHKIDENT As String
    Public Property MSGDATE As String
    Public Property USEREFUNB As String
    Public Property MSGFUNC As String
    Public Property MPIDMR As String
    Public Property MPIDMS As String
    Public Property ORDERSREF As String
    Public Property CREATEDDTE As String
    Public Property CREATEDBY As String
    Public Property CHANGEDDTE As String
    Public Property CHANGEDBY As String
    Public Property ZPNAME As String
    Public Property READDTE As String
    Public Property OBIS As String
    Public Property OBISTYPE As String
    Public Property AG As String
    Public Property AA As String
    Public Property READHW As String
    Public Property CCIACH As String
    Public Property CCI16 As String
    Public Property QTY As String
    Public Property QTYQUAL As String
    Public Property STATEADDINFO As String

    Public Sub New()
        Me.IDOCNR = ""
        Me.IDOCCONT = ""
        Me.EDITASK = ""
        Me.VERSION = ""
        Me.EDIREFUNB = ""
        Me.MSGREFUNH = ""
        Me.KAT = ""
        Me.CHKIDENT = ""
        Me.MSGDATE = ""
        Me.USEREFUNB = ""
        Me.MSGFUNC = ""
        Me.MPIDMR = ""
        Me.MPIDMS = ""
        Me.ORDERSREF = ""
        Me.CREATEDDTE = ""
        Me.CREATEDBY = ""
        Me.CHANGEDDTE = ""
        Me.CHANGEDBY = ""
        Me.ZPNAME = ""
        Me.READDTE = ""
        Me.OBIS = ""
        Me.OBISTYPE = ""
        Me.AG = ""
        Me.AA = ""
        Me.READHW = ""
        Me.CCIACH = ""
        Me.CCI16 = ""
        Me.QTY = ""
        Me.QTYQUAL = ""
        Me.STATEADDINFO = ""
    End Sub
    Public Sub New(ByVal IDOCNR As String)
        Me.New
        Me.IDOCNR = IDOCNR
    End Sub

    Public Function CreateParameters() As SqlClient.SqlParameter()

        Dim oParams As New List(Of SqlClient.SqlParameter)
        oParams.Add(New SqlClient.SqlParameter("@IDOCNR", Me.IDOCNR))
        oParams.Add(New SqlClient.SqlParameter("@IDOCCONT", Me.IDOCCONT))
        oParams.Add(New SqlClient.SqlParameter("@EDITASK", Me.EDITASK))
        oParams.Add(New SqlClient.SqlParameter("@VERSION", Me.VERSION))
        oParams.Add(New SqlClient.SqlParameter("@EDIREFUNB", Me.EDIREFUNB))
        oParams.Add(New SqlClient.SqlParameter("@MSGREFUNH", Me.MSGREFUNH))
        oParams.Add(New SqlClient.SqlParameter("@KAT", Me.KAT))
        oParams.Add(New SqlClient.SqlParameter("@CHKIDENT", Me.CHKIDENT))
        oParams.Add(New SqlClient.SqlParameter("@MSGDATE", Me.MSGDATE))
        oParams.Add(New SqlClient.SqlParameter("@USEREFUNB", Me.USEREFUNB))
        oParams.Add(New SqlClient.SqlParameter("@MSGFUNC", Me.MSGFUNC))
        oParams.Add(New SqlClient.SqlParameter("@MPIDMR", Me.MPIDMR))
        oParams.Add(New SqlClient.SqlParameter("@MPIDMS", Me.MPIDMS))
        oParams.Add(New SqlClient.SqlParameter("@ORDERSREF", Me.ORDERSREF))
        oParams.Add(New SqlClient.SqlParameter("@CREATEDDTE", Me.CREATEDDTE))
        oParams.Add(New SqlClient.SqlParameter("@CREATEDBY", Me.CREATEDBY))
        oParams.Add(New SqlClient.SqlParameter("@CHANGEDDTE", Me.CHANGEDDTE))
        oParams.Add(New SqlClient.SqlParameter("@CHANGEDBY", Me.CHANGEDBY))
        oParams.Add(New SqlClient.SqlParameter("@ZPNAME", Me.ZPNAME))
        oParams.Add(New SqlClient.SqlParameter("@READDTE", Me.READDTE))
        oParams.Add(New SqlClient.SqlParameter("@OBIS", Me.OBIS))
        oParams.Add(New SqlClient.SqlParameter("@OBISTYPE", Me.OBISTYPE))
        oParams.Add(New SqlClient.SqlParameter("@AG", Me.AG))
        oParams.Add(New SqlClient.SqlParameter("@AA", Me.AA))
        oParams.Add(New SqlClient.SqlParameter("@READHW", Me.READHW))
        oParams.Add(New SqlClient.SqlParameter("@CCIACH", Me.CCIACH))
        oParams.Add(New SqlClient.SqlParameter("@CCI16", Me.CCI16))
        oParams.Add(New SqlClient.SqlParameter("@QTY", Me.QTY))
        oParams.Add(New SqlClient.SqlParameter("@QTYQUAL", Me.QTYQUAL))
        oParams.Add(New SqlClient.SqlParameter("@STATEADDINFO", Me.STATEADDINFO))

        Return oParams.ToArray
    End Function

End Class
