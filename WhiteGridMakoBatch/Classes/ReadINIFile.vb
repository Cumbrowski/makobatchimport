﻿Imports System.Runtime.InteropServices
Imports System.Text

''' <summary>
''' Processing of .INI text configuration files in the format:
''' 
''' [SECTION1]
''' KEY1=VALUE1
''' KEY2=VALUE2
''' ;DISABLEDKEY3=VALUE3
''' 
''' [SECTION2]
''' KEY4=VALUE4
''' ...
''' ;Optional Comment Line
''' ...
''' 
''' With any lines starting with ';' being ignored as disabled or comment
''' 
''' This CLASS now also has the ability to WRITE/ADD/UPDATE .INI Content, where needed.
''' </summary>
''' <remarks>
''' Note: disabled/comment lines are not preserved, if this class is used for WRITING .INI values
''' 
''' Property for accessing entire INI content
''' <seealso>IniSections</seealso> is a custom Dictionary collection class
''' --------------------------------------
''' Public Property IniData As IniSections = Nothing
''' 
''' Reading Methods
''' --------------------------------------
''' Sub New(INIPath As String)
''' Function GetSectionNames() As String()
''' Function HasSection(ByVal section As String) As Boolean
''' Function SectionsWithEntry(ByVal entry As String) As String()
''' Function GetEntryNames(section As String) As String()
''' Function HasEntry(ByVal section As String, ByVal entry As String) As Boolean
''' Function GetEntryValue(section As String, entry As String) As Object
'''
''' Writing/Deleting/Adding Methods
''' --------------------------------------
''' Function WriteEntryValue(ByVal section As String, ByVal entry As String, ByVal value As Object) As Boolean
''' Function DeleteEntry(ByVal section As String, ByVal entry As String) As Boolean
''' Function DeleteSection(ByVal section As String) As Boolean
''' Function ReWriteIniFile(Optional ByVal NewIniFileName As String = "") As Boolean
'''
''' <example>
''' Sample for Reading an .INI File
''' <code>
''' Public Property Entry1 as Object
''' ...
''' Using oINI As New ReadINIFile(sFileName)
'''      Dim aSect As String() = oINI.GetSectionNames()
'''      For a As Integer = 0 To aSect.Length - 1
'''          Dim sVal As String = ""
'''          Select Case aSect(a).ToUpper
'''             Case "Section1".toUpper()
'''                 If oINI.HasEntry(aSect(a), "Entry1") Then
'''                      sVal = oINI.GetEntryValue(aSect(a), "Entry1")
'''                      If sVal.Trim &lt;&gt; "" Then My._Entry1 = sVal.Trim
'''                 End If
'''              '....
'''          End Select
'''      Next
''' End Using
''' </code>
''' </example>
''' <example>
''' Example for Writing an INI File
''' <code>
'''    Dim  oINI As New ReadINIFile(sNewINIFileName)
'''    oINI.WriteEntryValue("SECTION1","ENTRY1", Me._Entry1.toString)
'''    oINI.Dispose()
'''    oINI = Nothing
''' </code>
''' </example>
''' </remarks>
Public Class ReadINIFile
    Implements IDisposable

    ' First Method - Gathers the Value, as the SectionHeader and EntryKey are know.
    Private Declare Auto Function GetPrivateProfileString Lib "kernel32" (Section As String, Key As String, Value As String, _
                                                                         Result As StringBuilder, Size As Integer, _
                                                                         FileName As String) As Integer

    ' Second Method - Gathers a list of EntryKey for the known SectionHeader 
    Private Declare Auto Function GetPrivateProfileString Lib "kernel32" (Section As String, Key As Integer, Value As String, _
                                                                         <MarshalAs(UnmanagedType.LPArray)> Result As Byte(), Size As Integer, _
                                                                          FileName As String) As Integer

    ' Third Method - Gathers a list of SectionHeaders.
    Private Declare Auto Function GetPrivateProfileString Lib "kernel32" (Section As Integer, Key As String, Value As String, _
                                                                         <MarshalAs(UnmanagedType.LPArray)> Result As Byte(), Size As Integer, _
                                                                          FileName As String) As Integer
    ''' <summary>
    ''' Path and File Name location of the INI File to process
    ''' </summary>
    ''' <remarks></remarks>
    Private Path As String = ""

    ''' <summary>
    ''' Represents a [SECTION] within an INI File
    ''' </summary>
    ''' <remarks></remarks>
    Public Class IniSections
        Inherits Dictionary(Of String, IniValues)
        Public Sub New()
            MyBase.New(StringComparer.InvariantCultureIgnoreCase)
        End Sub

    End Class

    ''' <summary>
    ''' Represents a collection of none, one or multiple 
    ''' ENTRY=VALUE pairs within a single section block
    ''' </summary>
    ''' <remarks></remarks>
    Public Class IniValues
        Inherits Dictionary(Of String, Object)
        Public Sub New()
            MyBase.New(StringComparer.InvariantCultureIgnoreCase)
        End Sub

    End Class

    ''' <summary>
    ''' Dictionary property to allow access to the entire INI content directly
    ''' just in case, although the existing methods should cover everything 
    ''' that could be needed, but maybe I am mistaken about that :)
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property IniData As IniSections = Nothing

#Region "Constructor and Initial Reader"

    ''' <summary>
    ''' Initializes the class and parses the entire content of the INI file at the specified location
    ''' </summary>
    ''' <param name="INIPath">INI File Name and Path to Process</param>
    ''' <remarks>If <paramref>INIPath</paramref> is left empty (""), the class is still being created 
    ''' and can be used to Build a new INI file from scratch. 
    ''' However, the New (non-existing) INI File location should be passed anyway, 
    ''' to allow error-free outputs, once new INI data are being added.</remarks>
    Public Sub New(INIPath As String)
        Path = INIPath
        _IniData = New IniSections
        If IO.File.Exists(Path) Then
            Dim res As Integer
            Dim maxsize As Integer = 5000
            Dim bytes As Byte() = New Byte(maxsize - 1) {}
            Dim aSections() As String
            Dim aKeys() As String
            res = GetPrivateProfileString(0, "", "", bytes, maxsize, Path)
            Dim entries As String = Encoding.Unicode.GetString(bytes, 0, (res - (If(res > 0, 1, 0))) * 2)
            aSections = entries.Split(New Char() {ControlChars.NullChar})
            For a As Integer = 0 To aSections.Length - 1
                bytes = New Byte(maxsize - 1) {}
                res = GetPrivateProfileString(aSections(a), 0, "", bytes, maxsize, Path)
                entries = Encoding.Unicode.GetString(bytes, 0, (res - (If(res > 0, 1, 0))) * 2)
                aKeys = entries.Split(New Char() {ControlChars.NullChar})
                Dim oVal As New IniValues
                For b As Integer = 0 To aKeys.Length - 1
                    Dim sb As StringBuilder
                    sb = New StringBuilder(500)
                    res = GetPrivateProfileString(aSections(a), aKeys(b), "", sb, sb.Capacity, Path)
                    oVal.Add(aKeys(b), sb.ToString)
                Next
                _IniData.Add(aSections(a), oVal)
            Next
        End If
    End Sub
#End Region

#Region "INI Data Read/Check Methods"

    ''' <summary>
    ''' The Function called to obtain the SectionHeaders, and returns them in an Dynamic Array.
    ''' </summary>
    ''' <returns>String Array with the names of all [SECTIONS] within this INI File</returns>
    ''' <remarks></remarks>
    Public Function GetSectionNames() As String()
        Return _IniData.Keys.ToArray
    End Function
    ''' <summary>
    ''' Check whether the INI file has a [SECTION] with the specified name or not.
    ''' </summary>
    ''' <param name="section">Name of the [SECTION] to look for</param>
    ''' <returns>True, if such a section exists, False, if it does not</returns>
    ''' <remarks></remarks>
    Public Function HasSection(ByVal section As String) As Boolean
        Return _IniData.ContainsKey(section)
    End Function
    ''' <summary>
    ''' Returns an Array with all sections within the INI file that contain an Entry/Key with the specified name. 
    ''' This function is useful, if you want to determine, if a specific Entry/Key name might exist in one or multiple [SECTION] locations.
    ''' </summary>
    ''' <param name="entry">Entry/Key Name to search for across all [SECTION]s</param>
    ''' <returns>Array of Strings with matching [SECTION] names</returns>
    ''' <remarks></remarks>
    Public Function SectionsWithEntry(ByVal entry As String) As String()
        Dim lstSections As New List(Of String)
        For Each section In _IniData.Keys
            If _IniData.Item(section).ContainsKey(entry) Then
                lstSections.Add(section)
            End If
        Next
        Return lstSections.ToArray
    End Function

    ''' <summary>
    ''' The Function called to obtain the EntryKey's from the given SectionHeader string passed and returns them in an Dynamic Array
    ''' </summary>
    ''' <param name="section">Name of the [SECTION] to process</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetEntryNames(section As String) As String()
        If _IniData.ContainsKey(section) Then
            Return _IniData.Item(section).Keys.ToArray()
        Else
            Return Nothing
        End If
    End Function
    ''' <summary>
    ''' Checks whether the specified [SECTION] as the provided Entry/Key or not 
    ''' </summary>
    ''' <param name="section">Name of the [SECTION]</param>
    ''' <param name="entry">The Entry/Key to look for</param>
    ''' <returns>True, if it exists, False, if it does not, or if the [SECTION] does not exist as well</returns>
    ''' <remarks></remarks>
    Public Function HasEntry(ByVal section As String, ByVal entry As String) As Boolean
        If _IniData.ContainsKey(section) Then
            Return _IniData.Item(section).ContainsKey(entry)
        Else
            Return False
        End If
    End Function
    ''' <summary>
    ''' The Function called to obtain the EntryKey Value from the given SectionHeader and EntryKey string passed, then returned
    ''' </summary>
    ''' <param name="section">Name of the [SECTION]</param>
    ''' <param name="entry">Name of the Entry/Key</param>
    ''' <returns>Entry Value, if it exists or NOTHING, either the Entry AND/OR the [SECTION] do not exist</returns>
    ''' <remarks></remarks>
    Public Function GetEntryValue(section As String, entry As String) As Object
        If _IniData.ContainsKey(section) Then
            If _IniData.Item(section).ContainsKey(entry) Then
                Dim val = _IniData.Item(section).Item(entry)
                If Not String.IsNullOrEmpty(val) Then
                    If val.Trim.ToUpper = "TRUE" Or val.Trim.ToUpper = "FALSE" Then
                        Return CType(val.Trim.ToUpper.Equals("TRUE"), Boolean)
                    Else
                        Return val
                    End If
                Else
                    Return val
                End If
                Return _IniData.Item(section).Item(entry).ToString
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function
#End Region


#Region "INI Editing Methods"
    ''' <summary>
    ''' Writes the specified ENTRY=VALUE pair to the specified [SECTION]
    ''' If an entry within that section already exists, its value will be overwritten
    ''' If [SECTION] does not have ENTRY yet, it will be added, [SECTION] will be added
    ''' to the INI file as well, if it does not exist yet.
    ''' </summary>
    ''' <param name="section">The Name of the [SECTION] to Update/Add</param>
    ''' <param name="entry">The Entry Key Name to Update/Add</param>
    ''' <param name="value">The Entry Value to Overwrite or to Create</param>
    ''' <returns>True, if a change to the INI was necessary and the modified INI file was saved successfully</returns>
    ''' <remarks>If you want to keep the original INI file unchanged and write to a new and different INI file location instead, 
    ''' execute the <seealso>ReWriteIniFile</seealso> method prior this call and provide the optional <para>NewIniFileName</para> parameter 
    ''' with the new INI Path to write out the orininal INI content to this new location</remarks>
    Public Function WriteEntryValue(ByVal section As String, ByVal entry As String, ByVal value As Object) As Boolean
        Dim bValueChanged As Boolean = False
        If Not Me.HasSection(section) Then
            bValueChanged = True
            Dim oVal As IniValues = New IniValues
            _IniData.Add(section, oVal)
        End If
        If Not Me.HasEntry(section, entry) Then
            bValueChanged = True
            _IniData.Item(section).Add(entry, value)
        ElseIf Not Me.GetEntryValue(section, entry).Equals(value) Then
            bValueChanged = True
        End If

        If bValueChanged Then
            Return Me.ReWriteIniFile()
        Else
            Return False
        End If
    End Function
    ''' <summary>
    ''' Removes a spefific Entry key with the specified section 
    ''' (if the section AND the entry actually exist)
    ''' </summary>
    ''' <param name="section"></param>
    ''' <param name="entry"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DeleteEntry(ByVal section As String, ByVal entry As String) As Boolean
        Dim bValueChanged As Boolean = False
        If Me.HasEntry(section, entry) Then
            bValueChanged = Me._IniData.Item(section).Remove(entry)
            If bValueChanged Then
                bValueChanged = Me.ReWriteIniFile
            End If
        End If
        Return bValueChanged
    End Function

    ''' <summary>
    ''' Deletes an entire [SECTION] block from the INI
    ''' </summary>
    ''' <param name="section"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DeleteSection(ByVal section As String) As Boolean
        Dim bValueChanged As Boolean = False
        If Me.HasSection(section) Then
            bValueChanged = Me._IniData.Remove(section)
            If bValueChanged Then
                bValueChanged = Me.ReWriteIniFile
            End If
        End If
        Return bValueChanged
    End Function

    ''' <summary>
    ''' Writes out the entire INI, back to INI File. 
    ''' If <paramref>NewIniFileName</paramref> is specified, 
    ''' the original INI location will be changed to this new one
    ''' This write/output of the INI and all future updates 
    ''' are made to this new path from now on forward, 
    ''' unless a different path is again specified 
    ''' next time this method is called
    ''' </summary>
    ''' <param name="NewIniFileName">Optional, New INI File Location to Write to 
    ''' (now and in the future)</param>
    ''' <returns>True, If a new INI was written successfully.</returns>
    ''' <remarks>Method does not create a new INI file, if it is empty, however,  
    ''' it will delete the INI file, If all content was removed</remarks>
    Public Function ReWriteIniFile(Optional ByVal NewIniFileName As String = "") As Boolean
        Dim bSuccess As Boolean = False
        If NewIniFileName = "" Then NewIniFileName = Me.Path
        'INI Changed, write out new INI file with updated content
        Dim sOutput As String = ""
        For Each k As String In Me._IniData.Keys
            sOutput &= "[" & k & "]" & vbCrLf
            Dim oVals As IniValues = Me._IniData.Item(k)
            For Each e In oVals.Keys
                sOutput = e & "=" & oVals.Item(e).ToString & vbCrLf
            Next
            sOutput &= vbCrLf
        Next
        If sOutput <> "" And NewIniFileName <> "" Then
            Try
                If IO.File.Exists(NewIniFileName) Then System.IO.File.Delete(NewIniFileName)
                System.IO.File.WriteAllText(NewIniFileName, sOutput)
                bSuccess = True
                If Not Me.Path.Equals(NewIniFileName) Then Me.Path = NewIniFileName
            Catch ex As Exception
                bSuccess = False
            End Try
        Else
            If IO.File.Exists(NewIniFileName) Then
                System.IO.File.Delete(NewIniFileName)
                bSuccess = True
            End If
        End If
        Return bSuccess
    End Function

#End Region

#Region "IDisposable Support"
    Private disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                If Not IniData Is Nothing Then
                    Try
                        For Each k As String In IniData.Keys
                            IniData.Item(k) = Nothing
                            IniData.Remove(k)
                        Next
                    Catch ex As Exception
                    Finally
                        IniData = Nothing
                    End Try
                End If
                ' TODO: dispose managed state (managed objects).
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
