﻿Public Class UTILMDPARTNER
    'IDOCNR
    'PARTNERFUNC
    'NAME1
    'NAME2
    'NAMEADD1
    'NAMEADD2
    'TITLE
    'STREET1
    'STREET2
    'HOUSENO
    'POSTALCODE
    'CITY
    'COUNTRYCODE
    'MPID

    Public Property IDOCNR As String
    Public Property PARTNERFUNC As String
    Public Property NAME1 As String
    Public Property NAME2 As String
    Public Property NAMEADD1 As String
    Public Property NAMEADD2 As String
    Public Property TITLE As String
    Public Property STREET1 As String
    Public Property STREET2 As String
    Public Property HOUSENO As String
    Public Property POSTALCODE As String
    Public Property CITY As String
    Public Property COUNTRYCODE As String
    Public Property MPID As String
    Public Sub New()
        Me.IDOCNR = ""
        Me.PARTNERFUNC = ""
        Me.NAME1 = ""
        Me.NAME2 = ""
        Me.NAMEADD1 = ""
        Me.NAMEADD2 = ""
        Me.TITLE = ""
        Me.STREET1 = ""
        Me.STREET2 = ""
        Me.HOUSENO = ""
        Me.POSTALCODE = ""
        Me.CITY = ""
        Me.COUNTRYCODE = ""
        Me.MPID = ""
    End Sub
    Public Sub New(ByVal IDOCNR As String, ByVal PARTNERFUNC As String)
        Me.New
        Me.IDOCNR = IDOCNR
        Me.PARTNERFUNC = PARTNERFUNC
    End Sub
    Public Function CreateParameters() As SqlClient.SqlParameter()
        Dim oParams As New List(Of SqlClient.SqlParameter)
        oParams.Add(New SqlClient.SqlParameter("@IDOCNR", Me.IDOCNR))
        oParams.Add(New SqlClient.SqlParameter("@PARTNERFUNC", Me.PARTNERFUNC))
        oParams.Add(New SqlClient.SqlParameter("@NAME1", Me.NAME1))
        oParams.Add(New SqlClient.SqlParameter("@NAME2", Me.NAME2))
        oParams.Add(New SqlClient.SqlParameter("@NAMEADD1", Me.NAMEADD1))
        oParams.Add(New SqlClient.SqlParameter("@NAMEADD2", Me.NAMEADD2))
        oParams.Add(New SqlClient.SqlParameter("@TITLE", Me.TITLE))
        oParams.Add(New SqlClient.SqlParameter("@STREET1", Me.STREET1))
        oParams.Add(New SqlClient.SqlParameter("@STREET2", Me.STREET2))
        oParams.Add(New SqlClient.SqlParameter("@HOUSENO", Me.HOUSENO))
        oParams.Add(New SqlClient.SqlParameter("@POSTALCODE", Me.POSTALCODE))
        oParams.Add(New SqlClient.SqlParameter("@CITY", Me.CITY))
        oParams.Add(New SqlClient.SqlParameter("@COUNTRYCODE", Me.COUNTRYCODE))
        oParams.Add(New SqlClient.SqlParameter("@MPID", Me.MPID))
        Return oParams.ToArray
    End Function
End Class
