﻿Public Class UTILMDMSG
    'IDOCNR
    'IDOCCONT
    'EDITASK
    'VERSION
    'EDIREFUNB
    'MSGREFIDE
    'KAT
    'CHKIDENT
    'MSGDATE
    'UTCOFF
    'MPIDMS
    'MPIDMR
    'CONTACTIDMS
    'COMMNREMMS
    'ORDERSREF
    'MSGUSER

    Public Property IDOCNR As String
    Public Property IDOCCONT As String
    Public Property EDITASK As String
    Public Property VERSION As String
    Public Property EDIREFUNB As String
    Public Property MSGREFIDE As String
    Public Property KAT As String
    Public Property CHKIDENT As String
    Public Property MSGDATE As String
    Public Property UTCOFF As String
    Public Property MPIDMS As String
    Public Property MPIDMR As String
    Public Property CONTACTIDMS As String
    Public Property COMMNREMMS As String
    Public Property ORDERSREF As String
    Public Property MSGUSER As String
    Public Sub New()
        Me.IDOCNR = ""
        Me.IDOCCONT = ""
        Me.EDITASK = ""
        Me.VERSION = ""
        Me.EDIREFUNB = ""
        Me.MSGREFIDE = ""
        Me.KAT = ""
        Me.CHKIDENT = ""
        Me.MSGDATE = ""
        Me.UTCOFF = ""
        Me.MPIDMS = ""
        Me.MPIDMR = ""
        Me.CONTACTIDMS = ""
        Me.COMMNREMMS = ""
        Me.ORDERSREF = ""
        Me.MSGUSER = ""
    End Sub
    Public Sub New(ByVal IDOCNR As String)
        Me.New
        Me.IDOCNR = IDOCNR
    End Sub

    Public Function CreateParameters() As SqlClient.SqlParameter()
        Dim oParams As New List(Of SqlClient.SqlParameter)
        oParams.Add(New SqlClient.SqlParameter("@IDOCNR", Me.IDOCNR))
        oParams.Add(New SqlClient.SqlParameter("@IDOCCONT", Me.IDOCCONT))
        oParams.Add(New SqlClient.SqlParameter("@EDITASK", Me.EDITASK))
        oParams.Add(New SqlClient.SqlParameter("@VERSION", Me.VERSION))
        oParams.Add(New SqlClient.SqlParameter("@EDIREFUNB", Me.EDIREFUNB))
        oParams.Add(New SqlClient.SqlParameter("@MSGREFIDE", Me.MSGREFIDE))
        oParams.Add(New SqlClient.SqlParameter("@KAT", Me.KAT))
        oParams.Add(New SqlClient.SqlParameter("@CHKIDENT", Me.CHKIDENT))
        oParams.Add(New SqlClient.SqlParameter("@MSGDATE", Me.MSGDATE))
        oParams.Add(New SqlClient.SqlParameter("@UTCOFF", Me.UTCOFF))
        oParams.Add(New SqlClient.SqlParameter("@MPIDMS", Me.MPIDMS))
        oParams.Add(New SqlClient.SqlParameter("@MPIDMR", Me.MPIDMR))
        oParams.Add(New SqlClient.SqlParameter("@CONTACTIDMS", Me.CONTACTIDMS))
        oParams.Add(New SqlClient.SqlParameter("@COMMNREMMS", Me.COMMNREMMS))
        oParams.Add(New SqlClient.SqlParameter("@ORDERSREF", Me.ORDERSREF))
        oParams.Add(New SqlClient.SqlParameter("@MSGUSER", Me.MSGUSER))
        Return oParams.ToArray
    End Function
End Class
