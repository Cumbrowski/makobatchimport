﻿Public Class APERAK
    'IDOCNR
    'IDOCCONT
    'EDITASK
    'VERSION
    'EDIREFUNB
    'MSGREFUNH
    'KAT
    'CHKIDENT
    'MSGDATE
    'MPIDMR
    'MPIDMS
    'ORDERSREF
    'CREATEDDTE
    'CREATEDBY
    'CHANGEDDTE
    'CHANGEDBY
    'PROCIDENT
    'UNHREFSRC
    'UNBREFSRC
    'ERRCODE
    'ZPNAME
    'APERAKSTATUS
    'STATCHGDTE
    'STATCHGBY
    'APERAKTXTQUAL
    'TEXT1
    'TEXT2
    'TEXT3
    'TEXT4


    Public Property IDOCNR As String
    Public Property IDOCCONT As String
    Public Property EDITASK As String
    Public Property VERSION As String
    Public Property EDIREFUNB As String
    Public Property MSGREFUNH As String
    Public Property KAT As String
    Public Property CHKIDENT As String
    Public Property MSGDATE As String
    Public Property MPIDMR As String
    Public Property MPIDMS As String
    Public Property ORDERSREF As String
    Public Property CREATEDDTE As String
    Public Property CREATEDBY As String
    Public Property CHANGEDDTE As String
    Public Property CHANGEDBY As String
    Public Property PROCIDENT As String
    Public Property UNHREFSRC As String
    Public Property UNBREFSRC As String
    Public Property ERRCODE As String
    Public Property ZPNAME As String
    Public Property APERAKSTATUS As String
    Public Property STATCHGDTE As String
    Public Property STATCHGBY As String
    Public Property APERAKTXTQUAL As String
    Public Property TEXT1 As String
    Public Property TEXT2 As String
    Public Property TEXT3 As String
    Public Property TEXT4 As String

    Public Sub New()
        Me.IDOCNR = ""
        Me.IDOCCONT = ""
        Me.EDITASK = ""
        Me.VERSION = ""
        Me.EDIREFUNB = ""
        Me.MSGREFUNH = ""
        Me.KAT = ""
        Me.CHKIDENT = ""
        Me.MSGDATE = ""
        Me.MPIDMR = ""
        Me.MPIDMS = ""
        Me.ORDERSREF = ""
        Me.CREATEDDTE = ""
        Me.CREATEDBY = ""
        Me.CHANGEDDTE = ""
        Me.CHANGEDBY = ""
        Me.PROCIDENT = ""
        Me.UNHREFSRC = ""
        Me.UNBREFSRC = ""
        Me.ERRCODE = ""
        Me.ZPNAME = ""
        Me.APERAKSTATUS = ""
        Me.STATCHGDTE = ""
        Me.STATCHGBY = ""
        Me.APERAKTXTQUAL = ""
        Me.TEXT1 = ""
        Me.TEXT2 = ""
        Me.TEXT3 = ""
        Me.TEXT4 = ""
    End Sub
    Public Sub New(ByVal IDOCNR As String)
        Me.New
        Me.IDOCNR = IDOCNR
    End Sub

    Public Function CreateParameters() As SqlClient.SqlParameter()
        Dim oParams As New List(Of SqlClient.SqlParameter)
        oParams.Add(New SqlClient.SqlParameter("@IDOCNR", Me.IDOCNR))
        oParams.Add(New SqlClient.SqlParameter("@IDOCCONT", Me.IDOCCONT))
        oParams.Add(New SqlClient.SqlParameter("@EDITASK", Me.EDITASK))
        oParams.Add(New SqlClient.SqlParameter("@VERSION", Me.VERSION))
        oParams.Add(New SqlClient.SqlParameter("@EDIREFUNB", Me.EDIREFUNB))
        oParams.Add(New SqlClient.SqlParameter("@MSGREFUNH", Me.MSGREFUNH))
        oParams.Add(New SqlClient.SqlParameter("@KAT", Me.KAT))
        oParams.Add(New SqlClient.SqlParameter("@CHKIDENT", Me.CHKIDENT))
        oParams.Add(New SqlClient.SqlParameter("@MSGDATE", Me.MSGDATE))
        oParams.Add(New SqlClient.SqlParameter("@MPIDMR", Me.MPIDMR))
        oParams.Add(New SqlClient.SqlParameter("@MPIDMS", Me.MPIDMS))
        oParams.Add(New SqlClient.SqlParameter("@ORDERSREF", Me.ORDERSREF))
        oParams.Add(New SqlClient.SqlParameter("@CREATEDDTE", Me.CREATEDDTE))
        oParams.Add(New SqlClient.SqlParameter("@CREATEDBY", Me.CREATEDBY))
        oParams.Add(New SqlClient.SqlParameter("@CHANGEDDTE", Me.CHANGEDDTE))
        oParams.Add(New SqlClient.SqlParameter("@CHANGEDBY", Me.CHANGEDBY))
        oParams.Add(New SqlClient.SqlParameter("@PROCIDENT", Me.PROCIDENT))
        oParams.Add(New SqlClient.SqlParameter("@UNHREFSRC", Me.UNHREFSRC))
        oParams.Add(New SqlClient.SqlParameter("@UNBREFSRC", Me.UNBREFSRC))
        oParams.Add(New SqlClient.SqlParameter("@ERRCODE", Me.ERRCODE))
        oParams.Add(New SqlClient.SqlParameter("@ZPNAME", Me.ZPNAME))
        oParams.Add(New SqlClient.SqlParameter("@APERAKSTATUS", Me.APERAKSTATUS))
        oParams.Add(New SqlClient.SqlParameter("@STATCHGDTE", Me.STATCHGDTE))
        oParams.Add(New SqlClient.SqlParameter("@STATCHGBY", Me.STATCHGBY))
        oParams.Add(New SqlClient.SqlParameter("@APERAKTXTQUAL", Me.APERAKTXTQUAL))
        oParams.Add(New SqlClient.SqlParameter("@TEXT1", Me.TEXT1))
        oParams.Add(New SqlClient.SqlParameter("@TEXT2", Me.TEXT2))
        oParams.Add(New SqlClient.SqlParameter("@TEXT3", Me.TEXT3))
        oParams.Add(New SqlClient.SqlParameter("@TEXT4", Me.TEXT4))

        Return oParams.ToArray
    End Function


End Class
