﻿Public Class UTILMDCONTRACT
    'IDOCNR
    'NOTEPERIOD
    'TERMDTE
    'EARLYENDDTE
    'TERMDTECONTRACT
    'PLANTURNUSREAD
    'FRSTTURNUSREAD
    'TURNUSINTERVALL
    'TEXTARRAGE
    'TEXT1

    Public Property IDOCNR As String
    Public Property NOTEPERIOD As String
    Public Property TERMDTE As String
    Public Property EARLYENDDTE As String
    Public Property TERMDTECONTRACT As String
    Public Property PLANTURNUSREAD As String
    Public Property FRSTTURNUSREAD As String
    Public Property TURNUSINTERVALL As String
    Public Property TEXTARRAGE As String
    Public Property TEXT1 As String
    Public Sub New()
        Me.IDOCNR = ""
        Me.NOTEPERIOD = ""
        Me.TERMDTE = ""
        Me.EARLYENDDTE = ""
        Me.TERMDTECONTRACT = ""
        Me.PLANTURNUSREAD = ""
        Me.FRSTTURNUSREAD = ""
        Me.TURNUSINTERVALL = ""
        Me.TEXTARRAGE = ""
        Me.TEXT1 = ""
    End Sub
    Public Sub New(ByVal IDOCNR As String)
        Me.New
        Me.IDOCNR = IDOCNR
    End Sub
    Public Function CreateParameters() As SqlClient.SqlParameter()
        Dim oParams As New List(Of SqlClient.SqlParameter)
        oParams.Add(New SqlClient.SqlParameter("@IDOCNR", Me.IDOCNR))
        oParams.Add(New SqlClient.SqlParameter("@NOTEPERIOD", Me.NOTEPERIOD))
        oParams.Add(New SqlClient.SqlParameter("@TERMDTE", Me.TERMDTE))
        oParams.Add(New SqlClient.SqlParameter("@EARLYENDDTE", Me.EARLYENDDTE))
        oParams.Add(New SqlClient.SqlParameter("@TERMDTECONTRACT", Me.TERMDTECONTRACT))
        oParams.Add(New SqlClient.SqlParameter("@PLANTURNUSREAD", Me.PLANTURNUSREAD))
        oParams.Add(New SqlClient.SqlParameter("@FRSTTURNUSREAD", Me.FRSTTURNUSREAD))
        oParams.Add(New SqlClient.SqlParameter("@TURNUSINTERVALL", Me.TURNUSINTERVALL))
        oParams.Add(New SqlClient.SqlParameter("@TEXTARRAGE", Me.TEXTARRAGE))
        oParams.Add(New SqlClient.SqlParameter("@TEXT1", Me.TEXT1))
        Return oParams.ToArray
    End Function
End Class
