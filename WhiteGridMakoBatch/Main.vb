﻿Imports System
Imports System.IO
Imports WinSCP

Module WhiteGridMaKoBatch

    Dim DBConnectionString As String = ""
    Dim RemoteHost As String = ""
    Dim RemoteUser As String = ""
    Dim RemotePass As String = ""
    Dim ArchiveFiles As Boolean = False

    Sub Main()
        'Check if settings.ini file exists
        Dim INIFileName As String = Path.Combine(IO.Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory()), "settings.ini")
        If File.Exists(INIFileName) Then
            Call ReadIniFileSettings(INIFileName)
        End If


        'Process CommandLine Parameters
        Dim iArgs As Integer = My.Application.CommandLineArgs.Count()
        If iArgs > 0 Then
            For a As Integer = 0 To iArgs - 1
                Dim bNamedParam As Boolean = False
                Dim sNamedParam As String = ""
                Dim sParam As String = My.Application.CommandLineArgs.Item(a)
                If Microsoft.VisualBasic.Left(sParam, 1) = "/" And Microsoft.VisualBasic.InStr(sParam, ":", CompareMethod.Text) > 2 Then
                    sNamedParam = Strings.Left(sParam, Microsoft.VisualBasic.InStr(sParam, ":", CompareMethod.Text))
                    bNamedParam = True
                End If
                If bNamedParam = True Then
                    Dim sVal As String = Microsoft.VisualBasic.Right(sParam, Microsoft.VisualBasic.Len(sParam) - Microsoft.VisualBasic.Len(sNamedParam))
                    Select Case sNamedParam.ToUpper
                        Case "HOST"
                            RemoteHost = sVal.Trim
                        Case "USER"
                            RemoteUser = sVal.Trim
                        Case "PASS"
                            RemotePass = sVal.Trim
                        Case "CONN"
                            DBConnectionString = sVal.Trim

                    End Select
                End If
            Next
        End If


        Dim result As Integer = 0
        Dim sessionLogPath As String = Path.Combine(Environment.GetEnvironmentVariable("WEBJOBS_DATA_PATH"), Environment.GetEnvironmentVariable("WEBJOBS_RUN_ID"), "session.log")


        Try
            ' Setup session options
            Dim sessionOptions As New SessionOptions() With {
                .Protocol = Protocol.Sftp,
                .HostName = RemoteHost,
                .UserName = RemoteUser,
                .Password = RemotePass,
                .SshHostKeyFingerprint = "ssh-rsa 2048 xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx"
            }

            Using session As New Session()
                session.SessionLogPath = sessionLogPath

                ' Connect
                Console.WriteLine("Connecting...")
                session.Open(sessionOptions)

                Dim backupPath As String = "/home/user/backup/" + Environment.GetEnvironmentVariable("WEBJOBS_RUN_ID")
                session.CreateDirectory(backupPath)

                Console.WriteLine("Uploading...")
                Dim source As String = Path.Combine(Environment.GetEnvironmentVariable("WEBROOT_PATH"), "*")
                Dim target As String = backupPath & Convert.ToString("/")
                session.PutFiles(source, target).Check()



                Dim oDir As WinSCP.RemoteDirectoryInfo
                Dim oFile As WinSCP.RemoteFileInfo
                Dim sDir As String = ""
                oDir = session.ListDirectory(sDir)
                For a As Integer = 0 To oDir.Files.Count - 1
                    oFile = oDir.Files.Item(a)
                    If Not oFile.IsDirectory Then
                        Dim sFile As String = oFile.FullName
                        'session.GetFiles(sRemotePath, sLocalPath, remove:=True)
                    End If
                Next



            End Using

            result = 0
        Catch e As Exception
            Console.WriteLine("Error: {0}", e)
            result = 1
        End Try

        ' Dumping session log to Azure log (standard output) when it exists
        If File.Exists(sessionLogPath) Then
            Console.WriteLine("Session log:")

            Using sessionLog As Stream = File.OpenRead(sessionLogPath)
                Using standardOutput As Stream = Console.OpenStandardOutput()
                    sessionLog.CopyTo(standardOutput)
                End Using
            End Using
        End If
        Environment.ExitCode = result


    End Sub

    Public Sub ReadIniFileSettings(ByVal sFileName As String)
        'Read Settings
        Using oINI As New ReadINIFile(sFileName)
            Dim aSect As String() = oINI.GetSectionNames()
            For a As Integer = 0 To aSect.Length - 1
                Dim sVal As String = ""
                Select Case aSect(a).ToUpper
                    Case "CONNECTION"
                        If oINI.HasEntry(aSect(a), "RemoteHost") Then
                            sVal = oINI.GetEntryValue(aSect(a), "RemoteHost")
                            RemoteHost = sVal.Trim
                        End If
                        If oINI.HasEntry(aSect(a), "RemoteUser") Then
                            sVal = oINI.GetEntryValue(aSect(a), "RemoteUser")
                            RemoteUser = sVal.Trim
                        End If
                        If oINI.HasEntry(aSect(a), "RemotePassword") Then
                            sVal = oINI.GetEntryValue(aSect(a), "RemotePassword")
                            RemotePass = sVal.Trim
                        End If
                        If oINI.HasEntry(aSect(a), "RemoteSSHFingerPrint") Then
                            sVal = oINI.GetEntryValue(aSect(a), "RemoteSSHFingerPrint")
                        End If
                        If oINI.HasEntry(aSect(a), "MaKoDBConnection") Then
                            sVal = oINI.GetEntryValue(aSect(a), "MaKoDBConnection")
                            DBConnectionString = sVal.Trim
                        End If
                    Case "SETTINGS"
                        If oINI.HasEntry(aSect(a), "ArchiveFiles") Then
                            sVal = oINI.GetEntryValue(aSect(a), "ArchiveFiles")
                            If sVal.Trim <> "" Then
                                If sVal.Trim.ToUpper = "YES" Or sVal.Trim.ToUpper = "TRUE" Or sVal.Trim.ToUpper = "JA" Or sVal.Trim.ToUpper = "ON" Or sVal.Trim.ToUpper = "ENABLED" Then
                                    ArchiveFiles = True
                                End If
                            End If
                        End If
                End Select
            Next a
        End Using
    End Sub

    Public Sub ProcessFile(ByVal sFile As String)
        If File.Exists(sFile) Then
            Dim reader As StreamReader = My.Computer.FileSystem.OpenTextFileReader(sFile)
            Dim line As String

            Do
                line = reader.ReadLine


                '
                ' Code here
                '
            Loop Until line Is Nothing

            reader.Close()
        End If
    End Sub

    Public Sub ExecMSCONS()
        Using oTmpconnection As New System.Data.SqlClient.SqlConnection(DBConnectionString)
            oTmpconnection.Open()
            Using oTmpselectSystem As New System.Data.SqlClient.SqlCommand()
                oTmpselectSystem.Connection = oTmpconnection
                oTmpselectSystem.CommandText = "MSCONS_Insert"
                oTmpselectSystem.CommandType = CommandType.StoredProcedure
                oTmpselectSystem.Parameters.AddWithValue("@", "")
                Dim oParam As SqlClient.SqlParameter
                oParam = oTmpselectSystem.CreateParameter()
                oParam.Direction = ParameterDirection.Input
                oParam.ParameterName = ""



            End Using
        End Using
    End Sub

    Public Sub ExecuteSQL(ByVal sSQL As String, Optional ByVal bIgnoreErr As Boolean = False)
        If Strings.InStr(1, DBConnectionString, "Connection TimeOut", CompareMethod.Text) = 0 Then
            DBConnectionString &= "Connection Timeout=0;"
        End If
        Using oTmpconnection As New System.Data.SqlClient.SqlConnection(DBConnectionString)
            oTmpconnection.Open()
            Try
                Using oTmpselectSystem As New System.Data.SqlClient.SqlCommand(sSQL, oTmpconnection)
                    oTmpselectSystem.CommandTimeout = 0
                    oTmpselectSystem.ExecuteNonQuery()
                    oTmpselectSystem.Dispose()
                End Using
            Catch ex As System.Exception
                If Not bIgnoreErr Then
                    Throw New System.UnauthorizedAccessException("ERROR!")
                End If
            End Try
            oTmpconnection.Close()
        End Using
    End Sub

    Public Function GetIDOCNr() As String
        Dim sResult As String = ""
        Using oTmpconnection As New System.Data.SqlClient.SqlConnection(DBConnectionString)
            oTmpconnection.Open()
            Using oTmpselectSystem As New System.Data.SqlClient.SqlCommand()
                oTmpselectSystem.Connection = oTmpconnection
                oTmpselectSystem.CommandText = "GenerateIDOCNr"
                oTmpselectSystem.CommandType = CommandType.StoredProcedure
                Dim oReader As SqlClient.SqlDataReader = oTmpselectSystem.ExecuteReader()
                While oReader.Read
                    sResult = oReader("IDOCNR").ToString
                End While
            End Using
            oTmpconnection.Close()
        End Using
        Return sResult
    End Function


End Module
