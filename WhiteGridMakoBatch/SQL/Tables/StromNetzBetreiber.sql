SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StromNetzBetreiber](
	[Netzbetreibernummer] [int] NOT NULL,
	[Sitz] [varchar](255) NULL,
	[Unternehmen] [varchar](255) NULL,
	[PLZ] [varchar](50) NULL,
 CONSTRAINT [PK_StromNetzBetreiber] PRIMARY KEY CLUSTERED 
(
	[Netzbetreibernummer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO