SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PostleitzahlenDE](
	[Postleitzahl] [varchar](5) NOT NULL,
	[Ort] [nvarchar](128) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING ON

GO

/****** Object:  Index [IX_PostleitzahlenDE_Postleitzahl]    Script Date: 24.11.2016 08:19:13 ******/
CREATE NONCLUSTERED INDEX [IX_PostleitzahlenDE_Postleitzahl] ON [dbo].[PostleitzahlenDE]
(
	[Postleitzahl] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
