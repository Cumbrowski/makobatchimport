SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[NumberRanges](
	[RangeType] [int] NOT NULL,
	[RangeFrom] [bigint] NOT NULL,
	[RangeTo] [bigint] NOT NULL,
	[RangeCurrent] [bigint] NOT NULL,
 CONSTRAINT [PK_NumberRanges] PRIMARY KEY CLUSTERED 
(
	[RangeType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[NumberRanges] ADD  CONSTRAINT [DF_NumberRanges_RangeType]  DEFAULT ((0)) FOR [RangeType]
GO

ALTER TABLE [dbo].[NumberRanges] ADD  CONSTRAINT [DF_NumberRanges_RangeFrom]  DEFAULT ((0)) FOR [RangeFrom]
GO

ALTER TABLE [dbo].[NumberRanges] ADD  CONSTRAINT [DF_NumberRanges_RangeTo]  DEFAULT ((0)) FOR [RangeTo]
GO

ALTER TABLE [dbo].[NumberRanges] ADD  CONSTRAINT [DF_NumberRanges_RangeCurrent]  DEFAULT ((0)) FOR [RangeCurrent]
GO
