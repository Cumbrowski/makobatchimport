SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MaKo_Messages](
	[MsgNo] [nvarchar](64) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[MsgType] [int] NOT NULL,
	[Processed] [int] NOT NULL,
	[DateProcessed] [datetime] NOT NULL,
 CONSTRAINT [PK_MaKo_Messages] PRIMARY KEY CLUSTERED 
(
	[MsgNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MaKo_Messages] ADD  CONSTRAINT [DF_MaKo_Messages_MsgNo]  DEFAULT ('') FOR [MsgNo]
GO

ALTER TABLE [dbo].[MaKo_Messages] ADD  CONSTRAINT [DF_MaKo_Messages_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO

ALTER TABLE [dbo].[MaKo_Messages] ADD  CONSTRAINT [DF_MaKo_Messages_MsgType]  DEFAULT ((0)) FOR [MsgType]
GO

ALTER TABLE [dbo].[MaKo_Messages] ADD  CONSTRAINT [DF_MaKo_Messages_Processed]  DEFAULT ((0)) FOR [Processed]
GO

ALTER TABLE [dbo].[MaKo_Messages] ADD  CONSTRAINT [DF_MaKo_Messages_DateProcessed]  DEFAULT ('1/1/1900') FOR [DateProcessed]
GO