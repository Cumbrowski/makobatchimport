SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[NBPerPostalCode](
	[PostalCode] [varchar](5) NOT NULL,
	[NBNumber] [int] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[NBPerPostalCode] ADD  CONSTRAINT [DF_NBPerPostalCode_PostalCode]  DEFAULT ('') FOR [PostalCode]
GO

ALTER TABLE [dbo].[NBPerPostalCode] ADD  CONSTRAINT [DF_NBPerPostalCode_NBNumber]  DEFAULT ((0)) FOR [NBNumber]
GO
