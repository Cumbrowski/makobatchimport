SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PartnerFunction](
	[Attribute] [varchar](30) NOT NULL,
	[Value] [varchar](255) NOT NULL,
 CONSTRAINT [PK_PartnerFunction] PRIMARY KEY CLUSTERED 
(
	[Attribute] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PartnerFunction] ADD  CONSTRAINT [DF_PartnerFunction_Attribute]  DEFAULT ('') FOR [Attribute]
GO

ALTER TABLE [dbo].[PartnerFunction] ADD  CONSTRAINT [DF_PartnerFunction_Value]  DEFAULT ('') FOR [Value]
GO
