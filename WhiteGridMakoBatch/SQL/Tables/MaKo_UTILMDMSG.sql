SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MaKo_UTILMDMSG](
	[IDOCNR] [varchar](64) NOT NULL,
	[IDOCCONT] [varchar](64) NOT NULL,
	[EDITASK] [varchar](128) NOT NULL,
	[VERSION] [varchar](10) NOT NULL,
	[EDIREFUNB] [varchar](64) NOT NULL,
	[MSGREFIDE] [varchar](64) NOT NULL,
	[KAT] [varchar](5) NOT NULL,
	[CHKIDENT] [varchar](30) NOT NULL,
	[MSGDATE] [varchar](20) NOT NULL,
	[UTCOFF] [varchar](10) NOT NULL,
	[MPIDMS] [varchar](15) NOT NULL,
	[MPIDMR] [varchar](15) NOT NULL,
	[CONTACTIDMS] [varchar](128) NOT NULL,
	[COMMNREMMS] [varchar](128) NOT NULL,
	[ORDERSREF] [varchar](128) NOT NULL,
	[MSGUSER] [varchar](64) NOT NULL,
 CONSTRAINT [PK_MaKo_UTILMDMSG] PRIMARY KEY CLUSTERED 
(
	[IDOCNR] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MaKo_UTILMDMSG] ADD  CONSTRAINT [DF_MaKo_UTILMDMSG_IDOCNR]  DEFAULT ('') FOR [IDOCNR]
GO

ALTER TABLE [dbo].[MaKo_UTILMDMSG] ADD  CONSTRAINT [DF_MaKo_UTILMDMSG_IDOCCONT]  DEFAULT ('') FOR [IDOCCONT]
GO

ALTER TABLE [dbo].[MaKo_UTILMDMSG] ADD  CONSTRAINT [DF_MaKo_UTILMDMSG_EDITASK]  DEFAULT ('') FOR [EDITASK]
GO

ALTER TABLE [dbo].[MaKo_UTILMDMSG] ADD  CONSTRAINT [DF_MaKo_UTILMDMSG_VERSION]  DEFAULT ('') FOR [VERSION]
GO

ALTER TABLE [dbo].[MaKo_UTILMDMSG] ADD  CONSTRAINT [DF_MaKo_UTILMDMSG_EDIREFUNB]  DEFAULT ('') FOR [EDIREFUNB]
GO

ALTER TABLE [dbo].[MaKo_UTILMDMSG] ADD  CONSTRAINT [DF_MaKo_UTILMDMSG_MSGREFIDE]  DEFAULT ('') FOR [MSGREFIDE]
GO

ALTER TABLE [dbo].[MaKo_UTILMDMSG] ADD  CONSTRAINT [DF_MaKo_UTILMDMSG_KAT]  DEFAULT ('') FOR [KAT]
GO

ALTER TABLE [dbo].[MaKo_UTILMDMSG] ADD  CONSTRAINT [DF_MaKo_UTILMDMSG_CHKIDENT]  DEFAULT ('') FOR [CHKIDENT]
GO

ALTER TABLE [dbo].[MaKo_UTILMDMSG] ADD  CONSTRAINT [DF_MaKo_UTILMDMSG_MSGDATE]  DEFAULT ('') FOR [MSGDATE]
GO

ALTER TABLE [dbo].[MaKo_UTILMDMSG] ADD  CONSTRAINT [DF_MaKo_UTILMDMSG_UTCOFF]  DEFAULT ('') FOR [UTCOFF]
GO

ALTER TABLE [dbo].[MaKo_UTILMDMSG] ADD  CONSTRAINT [DF_MaKo_UTILMDMSG_MPIDMS]  DEFAULT ('') FOR [MPIDMS]
GO

ALTER TABLE [dbo].[MaKo_UTILMDMSG] ADD  CONSTRAINT [DF_MaKo_UTILMDMSG_MPIDMR]  DEFAULT ('') FOR [MPIDMR]
GO

ALTER TABLE [dbo].[MaKo_UTILMDMSG] ADD  CONSTRAINT [DF_MaKo_UTILMDMSG_CONTACTIDMS]  DEFAULT ('') FOR [CONTACTIDMS]
GO

ALTER TABLE [dbo].[MaKo_UTILMDMSG] ADD  CONSTRAINT [DF_MaKo_UTILMDMSG_COMMNREMMS]  DEFAULT ('') FOR [COMMNREMMS]
GO

ALTER TABLE [dbo].[MaKo_UTILMDMSG] ADD  CONSTRAINT [DF_MaKo_UTILMDMSG_ORDERSREF]  DEFAULT ('') FOR [ORDERSREF]
GO

ALTER TABLE [dbo].[MaKo_UTILMDMSG] ADD  CONSTRAINT [DF_Table_1_USER]  DEFAULT ('') FOR [MSGUSER]
GO