SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MaKo_UTILMDPARTNER](
	[IDOCNR] [varchar](64) NOT NULL,
	[PARTNERFUNC] [varchar](30) NOT NULL,
	[NAME1] [varchar](64) NOT NULL,
	[NAME2] [varchar](64) NOT NULL,
	[NAMEADD1] [varchar](64) NOT NULL,
	[NAMEADD2] [varchar](64) NOT NULL,
	[TITLE] [varchar](64) NOT NULL,
	[STREET1] [varchar](128) NOT NULL,
	[STREET2] [varchar](128) NOT NULL,
	[HOUSENO] [varchar](30) NOT NULL,
	[POSTALCODE] [varchar](10) NOT NULL,
	[CITY] [varchar](128) NOT NULL,
	[COUNTRYCODE] [varchar](3) NOT NULL,
	[MPID] [varchar](15) NOT NULL,
 CONSTRAINT [PK_MaKo_UTILMDPARTNER] PRIMARY KEY CLUSTERED 
(
	[IDOCNR] ASC,
	[PARTNERFUNC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MaKo_UTILMDPARTNER] ADD  CONSTRAINT [DF_MaKo_UTILMDPARTNER_IDOCNR]  DEFAULT ('') FOR [IDOCNR]
GO

ALTER TABLE [dbo].[MaKo_UTILMDPARTNER] ADD  CONSTRAINT [DF_MaKo_UTILMDPARTNER_PARTNERFUNC]  DEFAULT ('') FOR [PARTNERFUNC]
GO

ALTER TABLE [dbo].[MaKo_UTILMDPARTNER] ADD  CONSTRAINT [DF_MaKo_UTILMDPARTNER_NAME1]  DEFAULT ('') FOR [NAME1]
GO

ALTER TABLE [dbo].[MaKo_UTILMDPARTNER] ADD  CONSTRAINT [DF_MaKo_UTILMDPARTNER_NAME2]  DEFAULT ('') FOR [NAME2]
GO

ALTER TABLE [dbo].[MaKo_UTILMDPARTNER] ADD  CONSTRAINT [DF_MaKo_UTILMDPARTNER_NAMEADD1]  DEFAULT ('') FOR [NAMEADD1]
GO

ALTER TABLE [dbo].[MaKo_UTILMDPARTNER] ADD  CONSTRAINT [DF_MaKo_UTILMDPARTNER_NAMEADD2]  DEFAULT ('') FOR [NAMEADD2]
GO

ALTER TABLE [dbo].[MaKo_UTILMDPARTNER] ADD  CONSTRAINT [DF_MaKo_UTILMDPARTNER_TITLE]  DEFAULT ('') FOR [TITLE]
GO

ALTER TABLE [dbo].[MaKo_UTILMDPARTNER] ADD  CONSTRAINT [DF_MaKo_UTILMDPARTNER_STREET1]  DEFAULT ('') FOR [STREET1]
GO

ALTER TABLE [dbo].[MaKo_UTILMDPARTNER] ADD  CONSTRAINT [DF_MaKo_UTILMDPARTNER_STREET2]  DEFAULT ('') FOR [STREET2]
GO

ALTER TABLE [dbo].[MaKo_UTILMDPARTNER] ADD  CONSTRAINT [DF_MaKo_UTILMDPARTNER_HOUSENO]  DEFAULT ('') FOR [HOUSENO]
GO

ALTER TABLE [dbo].[MaKo_UTILMDPARTNER] ADD  CONSTRAINT [DF_MaKo_UTILMDPARTNER_POSTALCODE]  DEFAULT ('') FOR [POSTALCODE]
GO

ALTER TABLE [dbo].[MaKo_UTILMDPARTNER] ADD  CONSTRAINT [DF_MaKo_UTILMDPARTNER_CITY]  DEFAULT ('') FOR [CITY]
GO

ALTER TABLE [dbo].[MaKo_UTILMDPARTNER] ADD  CONSTRAINT [DF_MaKo_UTILMDPARTNER_COUNTRYCODE]  DEFAULT ('') FOR [COUNTRYCODE]
GO

ALTER TABLE [dbo].[MaKo_UTILMDPARTNER] ADD  CONSTRAINT [DF_MaKo_UTILMDPARTNER_MPID]  DEFAULT ('') FOR [MPID]
GO