SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MarktPartners](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RoleID] [int] NOT NULL,
	[MPID] [varchar](64) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Address1] [nvarchar](128) NOT NULL,
	[Address2] [nvarchar](128) NOT NULL,
	[PostalCode] [varchar](15) NOT NULL,
	[City] [nvarchar](64) NOT NULL,
	[ContactName] [nvarchar](128) NOT NULL,
	[ContactPhone] [varchar](35) NOT NULL,
	[ContactFax] [varchar](35) NOT NULL,
	[ContactEmail] [varchar](128) NOT NULL,
	[MaCoEmail] [varchar](128) NOT NULL,
	[NBNumber] [int] NOT NULL,
 CONSTRAINT [PK_MarktPartners] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MarktPartners] ADD  CONSTRAINT [DF_MarktPartners_RoleID]  DEFAULT ((0)) FOR [RoleID]
GO

ALTER TABLE [dbo].[MarktPartners] ADD  CONSTRAINT [DF_MarktPartners_MPID]  DEFAULT ('') FOR [MPID]
GO

ALTER TABLE [dbo].[MarktPartners] ADD  CONSTRAINT [DF_MarktPartners_Name]  DEFAULT ('') FOR [Name]
GO

ALTER TABLE [dbo].[MarktPartners] ADD  CONSTRAINT [DF_MarktPartners_Address1]  DEFAULT ('') FOR [Address1]
GO

ALTER TABLE [dbo].[MarktPartners] ADD  CONSTRAINT [DF_MarktPartners_Address2]  DEFAULT ('') FOR [Address2]
GO

ALTER TABLE [dbo].[MarktPartners] ADD  CONSTRAINT [DF_MarktPartners_PostalCode]  DEFAULT ('') FOR [PostalCode]
GO

ALTER TABLE [dbo].[MarktPartners] ADD  CONSTRAINT [DF_MarktPartners_City]  DEFAULT ('') FOR [City]
GO

ALTER TABLE [dbo].[MarktPartners] ADD  CONSTRAINT [DF_MarktPartners_ContactName]  DEFAULT ('') FOR [ContactName]
GO

ALTER TABLE [dbo].[MarktPartners] ADD  CONSTRAINT [DF_MarktPartners_ContactPhone]  DEFAULT ('') FOR [ContactPhone]
GO

ALTER TABLE [dbo].[MarktPartners] ADD  CONSTRAINT [DF_MarktPartners_ContactFax]  DEFAULT ('') FOR [ContactFax]
GO

ALTER TABLE [dbo].[MarktPartners] ADD  CONSTRAINT [DF_MarktPartners_ContactEmail]  DEFAULT ('') FOR [ContactEmail]
GO

ALTER TABLE [dbo].[MarktPartners] ADD  CONSTRAINT [DF_MarktPartners_MaCoEmail]  DEFAULT ('') FOR [MaCoEmail]
GO

ALTER TABLE [dbo].[MarktPartners] ADD  CONSTRAINT [DF_MarktPartners_NBNumber]  DEFAULT ((0)) FOR [NBNumber]
GO