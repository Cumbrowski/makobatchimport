SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[MSCONS_Insert]
	  @IDOCNR VARCHAR(64),
      @IDOCCONT VARCHAR(64),
      @EDITASK VARCHAR(128),
      @VERSION VARCHAR(10), 
      @EDIFEFUNB VARCHAR(64), 
      @MSGREFUNH VARCHAR(64), 
      @KAT VARCHAR(5), 
      @CHKIDENT VARCHAR(30), 
      @MSGDATE VARCHAR(20), 
      @USEREFUNB VARCHAR(5), 
      @MSGFUNC VARCHAR(10), 
      @MPIDMR VARCHAR(15), 
      @MPIDMS VARCHAR(15), 
      @ORDERSREF VARCHAR(128), 
      @CREATEDDTE VARCHAR(20), 
      @CREATEDBY VARCHAR(128), 
      @CHANGEDDTE VARCHAR(20), 
      @CHANGEDBY VARCHAR(128), 
      @ZPNAME VARCHAR(40), 
      @READDTE VARCHAR(20), 
      @OBIS VARCHAR(64), 
      @OBISTYPE VARCHAR(5), 
      @AG VARCHAR(128), 
      @AA VARCHAR(128), 
      @READHW VARCHAR(128), 
      @CCIACH VARCHAR(5), 
      @CCI16 VARCHAR(5), 
      @QTY VARCHAR(20), 
      @QTYQUAL VARCHAR(20), 
      @STATEADDINFO VARCHAR(max)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [MaKo_MSCONS] (
	   [IDOCNR]
      ,[IDOCCONT]
      ,[EDITASK]
      ,[VERSION]
      ,[EDIFEFUNB]
      ,[MSGREFUNH]
      ,[KAT]
      ,[CHKIDENT]
      ,[MSGDATE]
      ,[USEREFUNB]
      ,[MSGFUNC]
      ,[MPIDMR]
      ,[MPIDMS]
      ,[ORDERSREF]
      ,[CREATEDDTE]
      ,[CREATEDBY]
      ,[CHANGEDDTE]
      ,[CHANGEDBY]
      ,[ZPNAME]
      ,[READDTE]
      ,[OBIS]
      ,[OBISTYPE]
      ,[AG]
      ,[AA]
      ,[READHW]
      ,[CCIACH]
      ,[CCI16]
      ,[QTY]
      ,[QTYQUAL]
      ,[STATEADDINFO]
	  ) VALUES (
		@IDOCNr,
		@IDOCCONT,
		@EDITASK,
		@VERSION,
		@EDIFEFUNB,
		@MSGREFUNH,
		@KAT,
	    @CHKIDENT,
		@MSGDATE,
		@USEREFUNB,
		@MSGFUNC,
		@MPIDMR,
		@MPIDMS,
		@ORDERSREF,
		@CREATEDDTE,
		@CREATEDBY,
		@CHANGEDDTE,
		@CHANGEDBY,
		@ZPNAME,
		@READDTE,
		@OBIS,
		@OBISTYPE,
		@AG,
		@AA,
		@READHW,
		@CCIACH,
		@CCI16,
		@QTY,
		@QTYQUAL,
		@STATEADDINFO
	  );


  END