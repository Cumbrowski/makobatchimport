SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UTILMDCONTRACT_Select]
	@IDOCNR VARCHAR(64)
AS
BEGIN
	SET NOCOUNT ON;

SELECT [IDOCNR]
      ,[NOTEPERIOD]
      ,[TERMDTE]
      ,[EARLYENDDTE]
      ,[TERMDTECONTRACT]
      ,[PLANTURNUSREAD]
      ,[FRSTTURNUSREAD]
      ,[TURNUSINTERVALL]
      ,[TEXTARRAGE]
      ,[TEXT1]
  FROM [MaKo_UTILMDCONTRACT] WHERE [IDOCNR] = @IDOCNR
END
