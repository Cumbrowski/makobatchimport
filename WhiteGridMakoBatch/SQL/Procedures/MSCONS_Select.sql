SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[MSCONS_Select]
	@IDOCNr VARCHAR(64)
AS
BEGIN
	SET NOCOUNT ON;

SELECT [IDOCNR]
      ,[IDOCCONT]
      ,[EDITASK]
      ,[VERSION]
      ,[EDIFEFUNB]
      ,[MSGREFUNH]
      ,[KAT]
      ,[CHKIDENT]
      ,[MSGDATE]
      ,[USEREFUNB]
      ,[MSGFUNC]
      ,[MPIDMR]
      ,[MPIDMS]
      ,[ORDERSREF]
      ,[CREATEDDTE]
      ,[CREATEDBY]
      ,[CHANGEDDTE]
      ,[CHANGEDBY]
      ,[ZPNAME]
      ,[READDTE]
      ,[OBIS]
      ,[OBISTYPE]
      ,[AG]
      ,[AA]
      ,[READHW]
      ,[CCIACH]
      ,[CCI16]
      ,[QTY]
      ,[QTYQUAL]
      ,[STATEADDINFO]
  FROM [MaKo_MSCONS] WHERE [IDOCNR] = @IDOCNr;

  END