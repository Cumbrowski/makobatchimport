SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UTILMDPARTNER_Select]
	@IDOCNR	VARCHAR(64),
	@PARTNERFUNC VARCHAR(30) = NULL
AS
BEGIN
	SET NOCOUNT ON;

SELECT [IDOCNR]
      ,[PARTNERFUNC]
      ,[NAME1]
      ,[NAME2]
      ,[NAMEADD1]
      ,[NAMEADD2]
      ,[TITLE]
      ,[STREET1]
      ,[STREET2]
      ,[HOUSENO]
      ,[POSTALCODE]
      ,[CITY]
      ,[COUNTRYCODE]
      ,[MPID]
  FROM [MaKo_UTILMDPARTNER] WHERE [IDOCNR] = @IDOCNR AND
								  (@PARTNERFUNC IS NULL OR [PARTNERFUNC] = @PARTNERFUNC)
END