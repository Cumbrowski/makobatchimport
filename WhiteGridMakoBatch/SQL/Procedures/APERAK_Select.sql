SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[APERAK_Select]
	@IDOCNR VARCHAR(64)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT [IDOCNR]
      ,[IDOCCONT]
      ,[EDITASK]
      ,[VERSION]
      ,[EDIREFUNB]
      ,[MSGREFUNH]
      ,[KAT]
      ,[CHKIDENT]
      ,[MSGDATE]
      ,[MPIDMR]
      ,[MPIDMS]
      ,[ORDERSREF]
      ,[CREATEDDTE]
      ,[CREATEDBY]
      ,[CHANGEDDTE]
      ,[CHANGEDBY]
      ,[PROCIDENT]
      ,[UNHREFSRC]
      ,[UNBREFSRC]
      ,[ERRCODE]
      ,[ZPNAME]
      ,[APERAKSTATUS]
      ,[STATCHGDTE]
      ,[STATCHGBY]
      ,[APERAKTXTQUAL]
      ,[TEXT1]
      ,[TEXT2]
      ,[TEXT3]
      ,[TEXT4]
  FROM [MaKo_APERAK] WHERE [IDOCNR] = @IDOCNR;
END