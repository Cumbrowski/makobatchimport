SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GenerateIDOCNr]
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @IDOCNR     VARCHAR(64);
	DECLARE @D DATETIME = GETDATE(),
			@N BIGINT = 0,
		    @PART2 VARCHAR(14),
	        @PART3 VARCHAR(10);
	DECLARE @OUT TABLE(
				NEWNO BIGINT);
	BEGIN TRANSACTION
	SELECT @PART2 = FORMAT( @d, 'yyyyMMddhhmmss', 'en-US' ); 
	
	UPDATE NumberRanges SET RangeCurrent = RangeCurrent + 1
	       OUTPUT inserted.RangeCurrent 
	       INTO @OUT
	       WHERE RangeType = 1;
	SELECT @N = NEWNO FROM @OUT;
	SELECT @PART3 = FORMAT(@N,'0000000000');
	SELECT @IDOCNR = 'WG' + @PART2 + @PART3;
	COMMIT TRAN
	SELECT @IDOCNR AS IDOCNR;
END