SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UTILMDTECH_Select]
	@IDOCNR	VARCHAR(64)
AS
BEGIN
	SET NOCOUNT ON;

SELECT [IDOCNR]
      ,[ZPNAME]
      ,[VOLTLEVELDP]
      ,[SERIALNO]
      ,[OBIS]
      ,[TYPEREFSN]
      ,[CNTRNAME]
      ,[DECPLCPRECNT]
      ,[DECPLCSUFCNT]
      ,[CONVERTER]
      ,[CONVFACT]
      ,[COMM]
      ,[COMMTYPE]
      ,[CTRL]
      ,[CTRLTYPE]
      ,[SMGATE]
      ,[GATEADMIN]
      ,[CTRLBOX]
      ,[GASQUAL]
      ,[PRESSURELEVEL]
      ,[READMETHODM]
      ,[METERTYPE]
      ,[METERSIZEGAS]
      ,[RATECNT]
      ,[DIRECTION]
      ,[MOUNTTYPE]
      ,[ACQMEASVAL]
      ,[TRANSREASON]
      ,[RESPONSESTATUS]
  FROM [MaKo_UTILMDTECH] WHERE [IDOCNR] = @IDOCNR;
END
