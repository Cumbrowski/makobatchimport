SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UTILMDMSG_Insert]
	  @IDOCNR VARCHAR(64),
      @IDOCCONT	VARCHAR(64),
      @EDITASK VARCHAR(128),
      @VERSION VARCHAR(10),
      @EDIREFUNB VARCHAR(64),
      @MSGREFIDE VARCHAR(64),
      @KAT VARCHAR(5),
      @CHKIDENT VARCHAR(30),
      @MSGDATE VARCHAR(20),
      @UTCOFF VARCHAR(10),
      @MPIDMS VARCHAR(15),
      @MPIDMR VARCHAR(15),
      @CONTACTIDMS VARCHAR(128),
      @COMMNREMMS VARCHAR(128),
      @ORDERSREF VARCHAR(128),
      @MSGUSER VARCHAR(64)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [MaKo_UTILMDMSG] (
	   [IDOCNR]
      ,[IDOCCONT]
      ,[EDITASK]
      ,[VERSION]
      ,[EDIREFUNB]
      ,[MSGREFIDE]
      ,[KAT]
      ,[CHKIDENT]
      ,[MSGDATE]
      ,[UTCOFF]
      ,[MPIDMS]
      ,[MPIDMR]
      ,[CONTACTIDMS]
      ,[COMMNREMMS]
      ,[ORDERSREF]
      ,[MSGUSER]
	  ) VALUES (
		  @IDOCNR,
		  @IDOCCONT,
		  @EDITASK,
		  @VERSION,
		  @EDIREFUNB,
		  @MSGREFIDE,
		  @KAT,
		  @CHKIDENT,
		  @MSGDATE,
		  @UTCOFF,
		  @MPIDMS,
		  @MPIDMR,
		  @CONTACTIDMS,
		  @COMMNREMMS,
		  @ORDERSREF,
		  @MSGUSER
	  )
  
END