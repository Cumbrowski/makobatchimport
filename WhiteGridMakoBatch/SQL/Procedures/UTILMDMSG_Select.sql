SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UTILMDMSG_Select]
	@IDOCNR	VARCHAR(64)
AS
BEGIN
	SET NOCOUNT ON;

SELECT [IDOCNR]
      ,[IDOCCONT]
      ,[EDITASK]
      ,[VERSION]
      ,[EDIREFUNB]
      ,[MSGREFIDE]
      ,[KAT]
      ,[CHKIDENT]
      ,[MSGDATE]
      ,[UTCOFF]
      ,[MPIDMS]
      ,[MPIDMR]
      ,[CONTACTIDMS]
      ,[COMMNREMMS]
      ,[ORDERSREF]
      ,[MSGUSER]
  FROM [MaKo_UTILMDMSG] WHERE [IDOCNR] = @IDOCNR
END
