SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UTILMDPARTNER_Insert]
	@IDOCNR	VARCHAR(64),
    @PARTNERFUN	VARCHAR(30), 
    @NAME1 VARCHAR(64), 
    @NAME2 VARCHAR(64), 
    @NAMEADD1 VARCHAR(64), 
    @NAMEADD2 VARCHAR(64), 
    @TITLE VARCHAR(64), 
    @STREET1 VARCHAR(128), 
    @STREET2 VARCHAR(128), 
    @HOUSENO VARCHAR(30), 
    @POSTALCODE VARCHAR(10), 
    @CITY VARCHAR(128), 
    @COUNTRYCODE VARCHAR(3), 
    @MPID VARCHAR(15)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [MaKo_UTILMDPARTNER] ( 
	   [IDOCNR]
      ,[PARTNERFUNC]
      ,[NAME1]
      ,[NAME2]
      ,[NAMEADD1]
      ,[NAMEADD2]
      ,[TITLE]
      ,[STREET1]
      ,[STREET2]
      ,[HOUSENO]
      ,[POSTALCODE]
      ,[CITY]
      ,[COUNTRYCODE]
      ,[MPID]
	  ) VALUES (
		@IDOCNR,
		@PARTNERFUN,
		@NAME1,
		@NAME2,
		@NAMEADD1,
		@NAMEADD2,
		@TITLE,
		@STREET1,
		@STREET2,
		@HOUSENO,
		@POSTALCODE,
		@CITY,
		@COUNTRYCODE,
		@MPID
	  );
  
END
